## CONTENTS OF THIS FILE
   
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers
 
## INTRODUCTION

Flexy mod is a module developed to create a basic template for the website.
It will create content types,contents,custom blocks,view blocks,taxonomy etc.
This will reduce the frontend developers time.
They can customize their code by updating the layout & contents. 

 * For a full description of the module, visit the project page:
   [https://www.drupal.org/project/flexy_mod]

 * To submit bug reports and feature suggestions, or to track changes:
   [https://drupal.org/project/issues/flexy_mod]

## REQUIREMENTS

Flexy mod is a dependent module for Flexy theme.
<a href="https://www.drupal.org/project/flexy_theme">Flexy theme </a>

## INSTALLATION
 
 * Install as you would normally install a contributed Drupal module. Visit:
   [https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules]
   for further information.

## CONFIGURATION
Once the module installed: 
Two content types (works, about)
Sample nodes for that content type
Custom blocks (written inside src/Plugin/Block)
View blocks
Taxonomy will be created.
Blocks will be automatically placed on the proper region.

Apart from this,
frontend developers needs to disable some core regions from admin/stucture/block

## MAINTAINERS

Current maintainers:
 * [Rengaraj Srinivasan (rengaraj-srinivasan)](https://www.drupal.org//u/rengaraj95)


## This project has been sponsored by:
 * [Srijan Technologies](https://www.drupal.org/srijan-technologies)
