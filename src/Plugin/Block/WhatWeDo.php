<?php
/**
 * @file
 * Contains \Drupal\flexy_mod\Plugin\Block\XaiBlock.
 */

namespace Drupal\flexy_mod\Plugin\Block;

use Drupal\Core\Block\BlockBase;


/**
 * Provides a 'whatwedo' block.
 *
 * @Block(
 *   id = "what_we_do_block",
 *   admin_label = @Translation("What we do"),
 *   category = @Translation("Custom block")
 * )
 */
class WhatWeDo extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return array(
      '#type' => 'markup',
      '#markup' => '
      <div class="thingsido container">
        <div class="thingsido-overlay">
          <div class="left wow fadeInLeftBig" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="100" style="animation-duration: 0.5s; animation-delay: 0.5s; animation-name: fadeInLeftBig;">
            <p class="graphic">Graphic / Branding</p>
            <p class="drupal">Drupal Development</p>
            <p class="ui">UI / UX</p>
          </div>
          <div class="right wow fadeInRight" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="100" style="animation-duration: 0.5s; animation-delay: 0.5s; animation-name:fadeInRight;">
            <p class="web">Web Design</p>
            <p class="logo">Logo Design</p>
            <p class="photo">Photo Editing / Retouching</p>
          </div>
        </div>
      </div>
      ',
    );
  }
}