<?php
/**
 * @file
 * Contains \Drupal\flexy_mod\Plugin\Block\XaiBlock.
 */

namespace Drupal\flexy_mod\Plugin\Block;

use Drupal\Core\Block\BlockBase;


/**
 * Provides a 'Skills' block.
 *
 * @Block(
 *   id = "skills_block",
 *   admin_label = @Translation("Skills"),
 *   category = @Translation("Custom block")
 * )
 */
class Skills extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return array(
      '#type' => 'markup',
      '#markup' => '
      <div class="container">
        <div class="skills-wrp">
          <div class="col-md-6 wow fadeInLeftBig" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="100" id="skill-bar-wrapper" style="animation-duration: 0.5s; animation-delay: 0.5s; animation-name: fadeInLeftBig;">
            <div class="skillset">
            <p class="wow fadeInLeftBig" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="100" style="visibility: visible; animation-duration: 0.5s; animation-delay: 0.5s; animation-name: fadeInLeftBig;">Requirment Gathering</p>

            <p class="wow fadeInLeftBig" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="100" style="visibility: visible; animation-duration: 0.5s; animation-delay: 0.5s; animation-name: fadeInLeftBig;">Project Planning</p>

            <p class="wow fadeInLeftBig" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="100" style="visibility: visible; animation-duration: 0.5s; animation-delay: 0.5s; animation-name: fadeInLeftBig;">Agile &amp; Sprint Plan</p>

            <p class="wow fadeInLeftBig" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="100" style="visibility: visible; animation-duration: 0.5s; animation-delay: 0.5s; animation-name: fadeInLeftBig;">WireFrame Creation</p>

            <p class="wow fadeInLeftBig" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="100" style="visibility: visible; animation-duration: 0.5s; animation-delay: 0.5s; animation-name: fadeInLeftBig;">Web Design</p>

            <p class="wow fadeInLeftBig" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="100" style="visibility: visible; animation-duration: 0.5s; animation-delay: 0.5s; animation-name: fadeInLeftBig;">Graphic Design</p>

            <p class="wow fadeInLeftBig" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="100" style="visibility: visible; animation-duration: 0.5s; animation-delay: 0.5s; animation-name: fadeInLeftBig;">Front End Development</p>

            <p class="wow fadeInLeftBig" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="100" style="visibility: visible; animation-duration: 0.5s; animation-delay: 0.5s; animation-name: fadeInLeftBig;">Back End Development</p>

            <p class="wow fadeInLeftBig" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="100" style="visibility: visible; animation-duration: 0.5s; animation-delay: 0.5s; animation-name: fadeInLeftBig;">Web Hosting</p>
            </div>
          </div>

          <div class="col-md-6  wow fadeInRight" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="100" style="animation-duration: 0.5s; animation-delay: 0.5s; animation-name:fadeInRight;">
            <div class="developer-img"></div>
          </div>
        </div>
      </div>',
    );
  }
}