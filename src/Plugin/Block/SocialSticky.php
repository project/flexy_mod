<?php
/**
 * @file
 * Contains \Drupal\flexy_mod\Plugin\Block\XaiBlock.
 */

namespace Drupal\flexy_mod\Plugin\Block;

use Drupal\Core\Block\BlockBase;


/**
 * Provides a 'socialsticky' block.
 *
 * @Block(
 *   id = "social_sticky_block",
 *   admin_label = @Translation("Social Sticky block"),
 *   category = @Translation("Custom block")
 * )
 */
class SocialSticky extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return array(
      '#type' => 'markup',
      '#markup' => '
      <div class="favicons">
      <a class="phone" href="tel:XXXXXXXXX"><span class="fas fa-phone">&nbsp;</span></a> 
      <a class="whatsup" href="https://api.WhatsApp.com/send?phone=XXXXXXXXXX" target="_blank"><span class="fab fa-whatsapp">&nbsp;</span></a> 
      <a class="behance" href="https://www.behance.net" target="_blank"><span class="fab fa-behance">&nbsp;</span></a> 
      <a class="linkedin" href="https://www.linkedin.com/" target="_blank"><span class="fab fa-linkedin-in">&nbsp;</span></a> 
      <a class="facebook" href="https://www.facebook.com/" target="_blank"><span class="fab fa-facebook-f">&nbsp;</span></a>
      <a class="instagram" href="https://www.instagram.com" target="_blank"><span class="fab fa-instagram">&nbsp;</span></a></div>',
    );
  }
}