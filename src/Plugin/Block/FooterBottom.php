<?php
/**
 * @file
 * Contains \Drupal\flexy_mod\Plugin\Block\XaiBlock.
 */

namespace Drupal\flexy_mod\Plugin\Block;

use Drupal\Core\Block\BlockBase;


/**
 * Provides a 'footerbottom' block.
 *
 * @Block(
 *   id = "footer_bottom",
 *   admin_label = @Translation("Footer bottom block"),
 *   category = @Translation("Custom block")
 * )
 */
class FooterBottom extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return array(
      '#type' => 'markup',
      '#markup' => '
        <div class="footer-contact-wrap">
          <div class="caption">
            <p>Wanna know more <strong>about me</strong>, feel free to <a href="/contact"><strong>contact</strong></a></p>
          </div>
        </div>
        <div class="logo">
          <p class="logo-img"></p>
        </div>
        <div class="quotes">
        <p>XXXX - YYYY - ZZZZ</p>
        </div>
        <div class="footermenu"><a href="about">About</a> <a href="portfolio">Portfolio</a> <a href="blogs">Blogs</a> <a href="contact">Contact</a></div>
        <div class="favicons"><a href="https://www.behance.net" target="_blank"><span class="fab fa-behance">&nbsp;</span></a> <a href="https://www.linkedin.com/in" target="_blank"><span class="fab fa-linkedin-in">&nbsp;</span></a> <a href="https://api.WhatsApp.com/send?phone=XXXXXXXXXX" target="_blank"><span class="fab fa-whatsapp">&nbsp;</span></a> <a href="https://www.instagram.com" target="_blank"><span class="fab fa-instagram">&nbsp;</span></a> <a href="https://www.facebook.com" target="_blank"><span class="fab fa-facebook-f">&nbsp;</span></a> <a href="tel:XXXXXXXXXX"><span class="fas fa-phone">&nbsp;</span></a> <a href="mailto: xyz@gmail.com"><span class="fa fa-envelope-o">&nbsp;</span></a></div>
        <div class="copy-rights">Handcrafted by XXXX © 20XX</div>',
    );
  }
}