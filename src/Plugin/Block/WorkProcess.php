<?php
/**
 * @file
 * Contains \Drupal\flexy_mod\Plugin\Block\XaiBlock.
 */

namespace Drupal\flexy_mod\Plugin\Block;

use Drupal\Core\Block\BlockBase;


/**
 * Provides a 'Work Process' block.
 *
 * @Block(
 *   id = "work_process_block",
 *   admin_label = @Translation("Work process"),
 *   category = @Translation("Custom block")
 * )
 */
class WorkProcess extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return array(
      '#type' => 'markup',
      '#markup' => '
      <div class="work container">
        <div class="requirement col col-md-2 wow fadeInLeft" data-wow-delay="0.5s" data-wow-duration=".5s" data-wow-offset="100" style="visibility: visible; animation-duration: 0.5s; animation-delay: 0.5s; animation-name: fadeInLeftBig;"><span class="fa fa-list-alt">&nbsp;</span>
        <h2>Requirement</h2>
        </div>

        <div class="wireframe col col-md-2 wow fadeInLeft" data-wow-delay="0.5s" data-wow-duration=".5s" data-wow-offset="100" style="visibility: visible; animation-duration: 0.5s; animation-delay: 0.5s; animation-name: fadeInLeftBig;"><span class="fa fa-object-group">&nbsp;</span>

        <h2>Wire Frame</h2>
        </div>

        <div class="design col col-md-2 wow fadeInLeft" data-wow-delay="0.5s" data-wow-duration=".5s" data-wow-offset="100" style="visibility: visible; animation-duration: 0.5s; animation-delay: 0.5s; animation-name: fadeInLeftBig;"><span class="fa fa-paint-brush">&nbsp;</span>

        <h2>Design</h2>
        </div>

        <div class="development col col-md-2 wow fadeInLeft" data-wow-delay="0.5s" data-wow-duration=".5s" data-wow-offset="100" style="visibility: visible; animation-duration: 0.5s; animation-delay: 0.5s; animation-name: fadeInLeftBig;"><span class="fa fa-database">&nbsp;</span>

        <h2>Development</h2>
        </div>

        <div class="test col col-md-2 wow fadeInLeft" data-wow-delay="0.5s" data-wow-duration=".5s" data-wow-offset="100" style="visibility: visible; animation-duration: 0.5s; animation-delay: 0.5s; animation-name: fadeInLeftBig;"><span class="fa fa-bug">&nbsp;</span>

        <h2>Testing</h2>
        </div>

        <div class="lauch col col-md-2 wow fadeInLeft" data-wow-delay="0.5s" data-wow-duration=".5s" data-wow-offset="100" style="visibility: visible; animation-duration: 0.5s; animation-delay: 0.5s; animation-name: fadeInLeftBig;"><span class="fa fa-rocket">&nbsp;</span>

        <h2>Launch</h2>
        </div>
      </div>',
    );
  }
}