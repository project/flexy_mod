<?php
/**
 * @file
 * Contains \Drupal\flexy_mod\Plugin\Block\XaiBlock.
 */

namespace Drupal\flexy_mod\Plugin\Block;

use Drupal\Core\Block\BlockBase;


/**
 * Provides a 'topbanner' block.
 *
 * @Block(
 *   id = "top_banner_block",
 *   admin_label = @Translation("Top banner block"),
 *   category = @Translation("Custom block")
 * )
 */
class BannerBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return array(
      '#type' => 'markup',
      '#markup' => '
      <div class="banner">
        <div class="banner-overlay">
          <p class="wow bounceInDown" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="100" style="visibility: visible; animation-duration: 0.5s; animation-delay: 0.5s; animation-name: bounceInDown;">Hi we are <span>COMPANY/PERSON NAME</span></p>
          <p class="wow fadeInUp" data-wow-delay="0.8s" data-wow-duration="0.5s" data-wow-offset="100" style="visibility: visible; animation-duration: 0.5s; animation-delay: 0.8s; animation-name: fadeInUp;">Full Stack Web Developer</p>
          <div class="viewmore"><a class="wht" href="#navbar">Know More about us</a></div>
        </div>
      </div>',
    );
  }
}