<?php
/**
 * @file
 * Contains \Drupal\flexy_mod\Plugin\Block\XaiBlock.
 */

namespace Drupal\flexy_mod\Plugin\Block;

use Drupal\Core\Block\BlockBase;


/**
 * Provides a 'Expertise' block.
 *
 * @Block(
 *   id = "expertise_block",
 *   admin_label = @Translation("Expertise block"),
 *   category = @Translation("Custom block")
 * )
 */
class Expertise extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return array(
      '#type' => 'markup',
      '#markup' => '
      <div class="expert container">
        <div class="design col"><span class="fa fa-heart">&nbsp;</span>
          <h2>Design</h2>
          <p>98%</p>
        </div>
        <div class="ui col"><span class="fa fa-desktop">&nbsp;</span>
          <h2>Front-End</h2>
          <p>96%</p>
        </div>
        <div class="development col"><span class="fa fa-code">&nbsp;</span>
          <h2>Back-End</h2>
          <p>82%</p>
        </div>
      </div>',
    );
  }
}